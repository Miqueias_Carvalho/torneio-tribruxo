import "./App.css";
import bruxoImage from "./imagens/vass.png";
import { useState, useEffect } from "react";
import Students from "./components/Students";
import TelaInicial from "./components/TelaInicial";

function App() {
  // -----------------------------------------------
  //                    Estados
  //================================================
  //armazena todos os estudantes
  const [estudantes, setEstudantes] = useState([]);

  //verifica se a página está no estado inicial antes da primeira operação de escolha
  const [iniciandoPagina, setIniciandoPagina] = useState(true);

  // -----------------------------------------------
  //                  UseEffect
  //================================================
  useEffect(() => {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => setEstudantes(response))
      .catch((err) => console.log(err));
  }, []);

  // -------------------------------------------------
  //                      Funções
  // =================================================

  //retornando um array com 3 estudantes de casas distintas
  function selecionadosAleatorio(arrEstudantes) {
    //pegando primeiro estudante
    let primeiro =
      arrEstudantes[Math.floor(Math.random() * arrEstudantes.length)];

    //filtrando estudantes com casa diferente da do primeiro e armazenando em aux
    let aux = arrEstudantes.filter(
      (estudante) => estudante.house !== primeiro.house
    );

    //pegando segundo estudante a partir do array filtrado aux
    let segundo = aux[Math.floor(Math.random() * aux.length)];

    //filtrando estudantes com casa diferente da do primeiro e do segundo e armazenando em aux
    aux = aux.filter((estudante) => estudante.house !== segundo.house);

    //pegando terceiro estudante
    let terceiro = aux[Math.floor(Math.random() * aux.length)];

    //setEscolhidos([primeiro, segundo, terceiro]);
    return [primeiro, segundo, terceiro];
  }
  // ----------------------------------------------
  //                      JSX
  // ==============================================
  return (
    <div className="App">
      <main className="App-main">
        {iniciandoPagina ? (
          <TelaInicial setIniciandoPagina={setIniciandoPagina} />
        ) : (
          <Students
            estudantes={estudantes}
            selecionar={selecionadosAleatorio}
          />
        )}
        <div
          className={
            iniciandoPagina ? "imagem_anim" : "imagem_anim img_animation"
          }
        >
          <img src={bruxoImage} alt="" />
        </div>
      </main>
    </div>
  );
}

export default App;
