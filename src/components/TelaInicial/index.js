import "./style.css";

function TelaInicial({ setIniciandoPagina }) {
  return (
    <div className="telaInicial">
      <h2>Torneio Tribruxo</h2>
      <p>Clique no botão para encontrar os feiticeiros!</p>
      <div className="div_button">
        <button onClick={() => setIniciandoPagina(false)}>Começar</button>
      </div>
    </div>
  );
}
export default TelaInicial;
