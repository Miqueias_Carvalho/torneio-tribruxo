import "./style.css";

function StudentCards({ bruxo: { image, alive, house, name }, identificador }) {
  return (
    <div className={`personagem_card card_${house}`}>
      <div>
        <img src={image} alt=""></img>
      </div>
      <div>
        <p className="name">{name}</p>
        <p className={house}>{house}</p>
      </div>
    </div>
  );
}
export default StudentCards;
