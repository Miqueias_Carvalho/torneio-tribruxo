import "./style.css";
import { useState } from "react";
import StudentCard from "../StudentCards";

function Students({ estudantes, selecionar }) {
  // ----------------------------------------
  //                Estados
  //=========================================
  //inicializa 3 bruxos com a função selecionadosAleatorio
  const [escolhidos, setEscolhidos] = useState(selecionar(estudantes));

  // -----------------------------------------
  //                Funções
  //==========================================
  const novaSelecao = () => {
    let selecteds = selecionar(estudantes);
    setEscolhidos(selecteds);
  };

  // ------------------------------------------
  //                  JSX
  //===========================================
  return (
    <div>
      <div className="container_personagens">
        {escolhidos.map((bruxo, index) => (
          <StudentCard key={index} bruxo={bruxo} />
        ))}
      </div>
      <div className="div_button">
        <button onClick={novaSelecao}>Tentar Novamente</button>
      </div>
    </div>
  );
}
export default Students;
